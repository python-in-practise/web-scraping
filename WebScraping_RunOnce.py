import requests
import re
import csv
from bs4 import BeautifulSoup
import datetime
import schedule
import time
from pathlib import Path

link="https://www.worldometers.info/coronavirus/";	#Provide the URL u want scrap data from HTML tables

def getWebsiteData():
	now = datetime.datetime.now();
	#Get today's date
	currentFormattedDate=now.strftime("%Y-%m-%d");
	filename = "CoronaVirusCases_On_"+currentFormattedDate+".csv";		#Data to be exported file name

	data=requests.get(link)
	soup=BeautifulSoup(data.text);
	table=soup.findAll('table');			#get all tables

	for t in table:
		allTableRows=t.findAll('tr');

		headers=list();
		for h in allTableRows[0].findAll('th'):
			headers.append(h.text)		#add headers


		if(len(headers)==0):			#If <th> tag is not used for headers
			headers=list();
			for h in allTableRows[0].findAll('td'):
				headers.append(h.text);

		#Read all rows and export to CSV file	
		with open(filename, 'w') as csvfile:
			csvwriter = csv.writer(csvfile);
			csvwriter.writerow(headers);
			csvwriter.writerow(["Date",currentFormattedDate]);	#add date in a row
			for tableRow in allTableRows[1:]:
				rowValues=list();
				row=tableRow.findAll('td');
				for r in row:
					rowValues.append(r.text);
				csvwriter.writerow(rowValues);



getWebsiteData();

